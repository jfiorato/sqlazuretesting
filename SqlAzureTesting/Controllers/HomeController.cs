﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Practices.EnterpriseLibrary.WindowsAzure.TransientFaultHandling.AzureStorage;
using Microsoft.Practices.TransientFaultHandling;

namespace SqlAzureTesting.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            //var retryStrategy = new FixedInterval(3, TimeSpan.FromSeconds(1));
            //var retryPolicy = new RetryPolicy<StorageTransientErrorDetectionStrategy>(retryStrategy);

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            int i;
            for (i = 0; i < 50; i++)
            {
                var tables = new List<string>(new string[] { "person.person", "sales.salesorderdetail", "production.product", "sales.customer" });

                foreach (string table in tables)
                {
                    using (SqlConnection connection = new SqlConnection(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["MyDB"].ConnectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand("select * from " + table);
                        command.Connection = connection;

                        var result = command.ExecuteNonQuery();
                    }
                }
            }
            stopWatch.Stop();

            var ts = stopWatch.Elapsed;

            ViewBag.elapsed = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
            ViewBag.result = i;

            return View();
        }

    }
}
